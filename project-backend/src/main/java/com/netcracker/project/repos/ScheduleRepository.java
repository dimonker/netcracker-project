package com.netcracker.project.repos;

import com.netcracker.project.models.Schedule;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleRepository extends CrudRepository<Schedule, Long> {

    //
}
