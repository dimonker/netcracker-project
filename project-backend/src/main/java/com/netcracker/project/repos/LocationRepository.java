package com.netcracker.project.repos;

import com.netcracker.project.models.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Long> {

    // ?
}
