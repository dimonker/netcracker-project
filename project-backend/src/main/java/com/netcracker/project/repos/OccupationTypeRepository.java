package com.netcracker.project.repos;

import com.netcracker.project.models.OccupationType;
import org.springframework.data.repository.CrudRepository;

public interface OccupationTypeRepository extends CrudRepository<OccupationType, Long> {

    //
}
