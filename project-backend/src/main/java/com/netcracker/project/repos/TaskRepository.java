package com.netcracker.project.repos;

import com.netcracker.project.models.Task;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<Task, Long> {

    //
}
