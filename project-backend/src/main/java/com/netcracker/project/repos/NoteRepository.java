package com.netcracker.project.repos;

import com.netcracker.project.models.Note;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Calendar;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {

    Note findByDate(Calendar date);
    //
}
