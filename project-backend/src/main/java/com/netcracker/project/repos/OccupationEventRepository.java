package com.netcracker.project.repos;

import com.netcracker.project.models.OccupationEvent;
import org.springframework.data.repository.CrudRepository;

public interface OccupationEventRepository extends CrudRepository<OccupationEvent, Long> {

    //
}
