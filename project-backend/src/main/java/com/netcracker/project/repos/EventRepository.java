package com.netcracker.project.repos;

import com.netcracker.project.models.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {

    //
}
