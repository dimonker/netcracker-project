package com.netcracker.project.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/hello", headers = "Accept=text/html")
public class HelloWorldController {

    @GetMapping
    public ResponseEntity hello(){
        return new ResponseEntity("Hello world", HttpStatus.OK);
    }
}
