package com.netcracker.project.models;

public enum Status {
    DONE,
    OPEN,
    MOVED
}
