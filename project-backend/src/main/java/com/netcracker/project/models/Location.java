package com.netcracker.project.models;

import javax.persistence.*;

@Entity
@Table (name = "locations")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private float latitude;

    private float longitude;

    private String description;

    public Location() {
    }

}
