package com.netcracker.project.models;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table (name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private String description;

    private float estimatedDuration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "occupation_type_id")
    private OccupationType occupationType;

    private Status status;

    private Priority priority;

    private Calendar deadlineTimestamp;

    private int repeatedTimes;

    public Task() {
    }
}
