package com.netcracker.project.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String email;

    private long hash_password;

    private String first_name;

    private String middle_name;

    private String last_name;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Event> events;

    public User() {
    }

    @Override
    public String toString() {
        return "models.User{" +
                "id=" + id +
                ", first name='" + first_name + '\'' +
                ", last name=" + last_name +
                '}';
    }
}
