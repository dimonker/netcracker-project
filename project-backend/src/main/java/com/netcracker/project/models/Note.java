package com.netcracker.project.models;

import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table (name = "notes")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Type(type = "calendar_date")
    private Calendar date;

    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Note() {
    }

    public Note(Calendar date, String text) {
        this.date = date;
        this.text = text;
    }
}
