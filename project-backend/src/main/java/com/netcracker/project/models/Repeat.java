package com.netcracker.project.models;

public enum Repeat {
    DAILY,
    WEEKLY,
    BI_WEEKLY,
    EVERY_MONTH,
    EVERY_YEAR
}
