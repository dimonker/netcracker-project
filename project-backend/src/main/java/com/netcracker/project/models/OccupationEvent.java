package com.netcracker.project.models;

import javax.persistence.*;

@Entity
@Table (name = "occupation_events")
public class OccupationEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.LAZY)   // ?
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    public OccupationEvent() {
    }
}
