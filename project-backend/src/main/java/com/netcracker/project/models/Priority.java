package com.netcracker.project.models;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW
}
