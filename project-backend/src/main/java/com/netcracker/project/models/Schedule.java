package com.netcracker.project.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Time;
import java.util.Calendar;

@Entity
@Table (name = "schedule")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Repeat repeat;

    @Type(type = "calendar_date")
    private Calendar startDate;

    @Type(type = "calendar_date")
    private Calendar endDate;

    private Time startTime;

    private Time endTime;

    public Schedule() {
    }
}
