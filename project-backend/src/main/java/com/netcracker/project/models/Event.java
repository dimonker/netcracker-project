package com.netcracker.project.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table (name = "events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private String description;

    @OneToOne(fetch = FetchType.LAZY)   // ?
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private Location location;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "occupation_type_id")
    private OccupationType occupationType;

    @Type(type = "calendar")
    private Calendar created_timestamp;

    @Type(type = "calendar")
    private Calendar updated_timestamp;

    private boolean busy;

    public Event() {
    }
}

