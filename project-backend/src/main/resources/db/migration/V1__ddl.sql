-- auto-generated definition
create table locations
(
    id          serial       not null
        constraint locations_pk
            primary key,
    name        varchar(128) not null,
    latitude    real         not null,
    longitude   real         not null,
    description varchar(200)
);

alter table locations
    owner to postgres;

-- auto-generated definition
create table occupation_types
(
    id    serial       not null
        constraint occupation_types_pk
            primary key,
    name  varchar(128) not null,
    color varchar(10)
);

alter table occupation_types
    owner to postgres;

-- auto-generated definition
create table schedule
(
    id         serial not null
        constraint schedule_pk
            primary key,
    repeat     integer,
    start_date date   not null,
    end_date   date   not null,
    start_time time   not null,
    end_time   time   not null
);

alter table schedule
    owner to postgres;

-- auto-generated definition
create table users
(
    id            serial       not null
        constraint users_pk
            primary key,
    email         varchar(128) not null,
    hash_password bigint       not null,
    first_name    varchar(128) not null,
    middle_name   varchar(128),
    last_name     varchar(128)
);

alter table users
    owner to postgres;

-- auto-generated definition
create table notes
(
    id      serial  not null
        constraint notes_pk
            primary key,
    date    date    not null,
    text    varchar(200),
    user_id integer not null
        constraint notes_users_id_fk
            references users
            on update cascade on delete cascade
);

alter table notes
    owner to postgres;

-- auto-generated definition
create table events
(
    id                 serial       not null
        constraint events_pk
            primary key,
    name               varchar(128) not null,
    user_id            integer      not null
        constraint events_users_id_fk
            references users
            on update cascade on delete cascade,
    description        varchar(200),
    schedule_id        integer      not null
        constraint events_schedule_id_fk
            references schedule
            on update cascade on delete cascade,
    location_id        integer
        constraint events_locations_id_fk
            references locations
            on update cascade on delete cascade,
    occupation_type_id integer
        constraint events_occupation_types_id_fk
            references occupation_types
            on update cascade on delete cascade,
    created_timestamp  timestamp,
    updated_timestamp  timestamp,
    busy               boolean
);

alter table events
    owner to postgres;

-- auto-generated definition
create table tasks
(
    id                 serial       not null
        constraint tasks_pk
            primary key,
    name               varchar(128) not null,
    user_id            integer      not null
        constraint tasks_users_id_fk
            references users
            on update cascade on delete cascade,
    description        varchar(200),
    estimated_duration real,
    schedule_id        integer
        constraint tasks_schedule_id_fk
            references schedule
            on update cascade on delete cascade,
    occupation_type_id integer
        constraint tasks_occupation_types_id_fk
            references occupation_types
            on update cascade on delete cascade,
    deadline_timestamp timestamp,
    repeated_times     integer,
    status             integer      not null,
    priority           integer      not null
);

alter table tasks
    owner to postgres;

-- auto-generated definition
create table occupation_events
(
    id          serial  not null
        constraint occupation_events_pk
            primary key,
    schedule_id integer not null
        constraint occupation_events_schedule_id_fk
            references schedule
            on update cascade on delete cascade
);

alter table occupation_events
    owner to postgres;