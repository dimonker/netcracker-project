## Package project jar

```bash
mvn clean package
```

## Run profect jar

```bash
java -jar project/target/project-0.0.1-SNAPSHOT.jar
```
