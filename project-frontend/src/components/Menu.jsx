import React, { Component } from 'react'
import menuImage from '../images/menu.png'
import styled from 'styled-components'

const MenuIcon = styled.img`
  height: 60px;
  width: auto;
`

class Menu extends Component {
  render() {
    return (
      <div>
        <MenuIcon src={menuImage}></MenuIcon>
      </div>
    )
  }
}

export default Menu
