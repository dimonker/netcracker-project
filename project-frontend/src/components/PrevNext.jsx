import React from 'react'
import arrow from '../images/arrow.png'
import styled from 'styled-components'

const ArrowIcon = styled.img`
  height: 40px;
  transform: ${props => (props.left ? 'rotate(180deg)' : 'rotate(0deg)')};
  border: 1px solid black;
  ${props => (props.left ? 'border-left-width: 0px;' : '')}
`

const PrevNext = () => {
  return (
    <div>
      <ArrowIcon left src={arrow} alt="arrow"></ArrowIcon>

      <ArrowIcon src={arrow} alt="arrow"></ArrowIcon>
    </div>
  )
}

export default PrevNext
