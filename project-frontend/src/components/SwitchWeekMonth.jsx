import React, { PureComponent } from 'react'
import styled from 'styled-components'

const Btn = styled.button`
  display: inline-block;
  border: 1px solid black;
  padding: 5px 32px;

  &:last-of-type {
    border-left-width: 0px;
  }
`

const SwitchWeekMonth = () => {
  return (
    <div>
      <Btn>Week</Btn>
      <Btn>Month</Btn>
    </div>
  )
}

export default SwitchWeekMonth
