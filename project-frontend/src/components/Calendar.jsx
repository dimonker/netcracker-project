import React, { PureComponent } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import moment from 'moment'
import Draggable from 'react-draggable'
import { relative } from 'path'

const getShortWeekDays = () => moment.weekdaysShort(true)

const getWeekDays = currentWeek => {
  const from = currentWeek
  const to = currentWeek.clone().endOf('isoWeek')

  var days = []
  var day = from

  while (day <= to) {
    days.push(day)
    day = day.clone().add(1, 'd')
  }

  return days
}

const Container = styled.div`
  display: grid;
  width: 100%;
  /* grid-template-columns: repeat(7, minmax(120px, 1fr)); */
  grid-template-columns: repeat(7, 160px);

  grid-template-rows: 60px;
  overflow: auto;
`

const DayHourContainer = styled(Container)`
  height: calc(24 * 60px);
  position: relative;
`

const Box = styled.div`
  background: #fff;
  border: 1px solid #999;
  border-radius: 3px;
  width: 135px;
  height: 60px fit-content;
  padding: 10px;
  float: left;
  cursor: move;
  position: absolute;
  background: grey;
  color: white;
`
const DayNumber = styled.div`
  font-size: 12px;
  text-transform: uppercase;
  color: #99a1a7;
  text-align: center;
  border-bottom: 1px solid rgba(166, 168, 179, 0.12);
  line-height: 50px;
  font-weight: 500;
`

const DayHour = styled.div`
  text-align: right;
  letter-spacing: 1px;
  font-size: 12px;
  box-sizing: border-box;
  color: #98a0a6;
  pointer-events: none;
  z-index: 1;
  height: 60px;
  border-bottom: 1px solid rgba(166, 168, 179, 0.12);
  border-right: 1px solid rgba(166, 168, 179, 0.12);
`

class DayHourComponent extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  state = {
    elements: [],
  }

  handleClick() {
    console.log('clicked')
    this.setState(state => {
      return {
        elements: state.elements
          .clone()
          .push(<DraggableWithPositon></DraggableWithPositon>),
      }
    })
  }

  render() {
    return <DayHour onClick={this.handleClick}>{this.elements}</DayHour>
  }
}

class DraggableWithPositon extends React.Component {
  state = {
    deltaPosition: {
      x: 0,
      y: 0,
    },
  }

  handleDrag = (e, ui) => {
    const { x, y } = this.state.deltaPosition
    this.setState({
      deltaPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      },
    })
  }

  render() {
    const { deltaPosition } = this.state
    return (
      <Draggable
        bounds="parent"
        grid={[160, 15]}
        onDrag={this.handleDrag}
        defaultPosition={this.props.defaultPosition}
      >
        <Box className="box">
          x: {deltaPosition.x}, y: {deltaPosition.y}
          {this.props.children}
        </Box>
      </Draggable>
    )
  }
}

class Con extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e) {
    e.preventDefault()
    console.log('Con clicked')
    console.log(e.currentTarget)
    console.log(e.target.id)
  }

  render() {
    return (
      <div
        style={{ height: '740px', overflow: 'auto' }}
        onClick={this.handleClick}
      >
        {this.props.children}
      </div>
    )
  }
}

class Calendar extends React.Component {
  render() {
    const currentWeek = moment().startOf('isoWeek')
    const daysNumbers = getWeekDays(currentWeek).map(e => e.date())
    const days = []
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 24; j++) {
        days.push(<DayHour></DayHour>)
      }
    }

    return (
      <div style={{ height: '800px' }}>
        <Container>
          {daysNumbers.map(e => (
            <DayNumber>{e}</DayNumber>
          ))}
        </Container>
        <Con>
          <DayHourContainer>
            {days}

            <DraggableWithPositon>
              <div>Some text</div>
            </DraggableWithPositon>
            <DraggableWithPositon defaultPosition={{ x: 160, y: 60 }}>
              <div>TEXT</div>
            </DraggableWithPositon>
          </DayHourContainer>
        </Con>
      </div>
    )
  }
}

export default Calendar
