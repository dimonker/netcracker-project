import React from 'react'
import Menu from './components/Menu'
import styled from 'styled-components'
import moment from 'moment'
import PrevNext from './components/PrevNext'
import SwitchWeekMonth from './components/SwitchWeekMonth'
import Calendar from './components/Calendar'
import Draggable from 'react-draggable'
const TopPanel = styled.div`
  height: 80px;
  display: flex;
  align-items: center;
  padding: 0 16px;
  justify-content: space-between;
`

const Today = styled.div`
  font-size: 1.5em;
`

const Container = styled.div`
  /* width: 90%; */
  background: white;
  box-shadow: 0 2px 20px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  margin: 15px;
  float: right;
`

const App = () => {
  return (
    <Container>
      <TopPanel>
        <Today>{moment().format('LL')}</Today>
        <PrevNext></PrevNext>
        <SwitchWeekMonth></SwitchWeekMonth>
      </TopPanel>
      <Calendar></Calendar>
    </Container>
  )
}

export default App
