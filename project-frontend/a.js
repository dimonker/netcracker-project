const moment = require('moment')

moment.updateLocale('en', {
  week: {
    dow: 1,
  },
})

const getShortWeekDays = () => moment.weekdaysShort(true)

const getWeekDays = currentWeek => {
  const from = currentWeek
  const to = currentWeek.clone().endOf('isoWeek')

  var days = []
  var day = from

  while (day <= to) {
    days.push(day)
    day = day.clone().add(1, 'd')
  }

  return days
}

// console.log(getShortWeekDays())
// console.log(moment.weekdays(true))
console.log(getWeekDays(moment().startOf('isoWeek')).map(e => e.date()))
